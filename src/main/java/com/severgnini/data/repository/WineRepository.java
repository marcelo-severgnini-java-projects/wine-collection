package com.severgnini.data.repository;

import com.severgnini.data.Wine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("wineRepository")
public interface WineRepository extends JpaRepository<Wine, Long> {
    Wine findByTitle(String title);
}
