package com.severgnini.service.impl;

import com.severgnini.data.Wine;
import com.severgnini.data.repository.WineRepository;
import com.severgnini.service.WineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.util.List;


@Service("wineService")
public class WineServiceImpl implements WineService {

    private WineRepository wineRepository;

    @Autowired
    public WineServiceImpl(@Qualifier("wineRepository") WineRepository wineRepository) {
        this.wineRepository = wineRepository;
    }

    @Override
    public List<Wine> findAll() {
        return this.wineRepository.findAll();
    }
}
