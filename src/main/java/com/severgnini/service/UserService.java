package com.severgnini.service;

import com.severgnini.data.User;

public interface UserService {
    User findUserByEmail(String email);
    void saveUser(User user);
}
