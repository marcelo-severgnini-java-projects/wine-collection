package com.severgnini.service;

import com.severgnini.data.Wine;

import java.util.List;

public interface WineService {
    List<Wine> findAll();
}
