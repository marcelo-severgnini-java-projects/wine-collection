# Wine Collection (WIP)

[![pipeline status](https://gitlab.com/marcelo-severgnini-java-projects/wine-collection/badges/master/pipeline.svg)](https://gitlab.com/marcelo-severgnini-java-projects/wine-collection/commits/master)
[![MIT Licensed](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/marcelosevergnini/wine-collection/master/LICENSE)

Web Application to store a collection of wines.

TechStack
   * This app uses SpringBoot as core base.
   * Spring MVC for rendering routes and views. 
   * Thymeleaf as view engine
   * Spring Data (JPA/Hibernate) as data layer.
   * Spring Security for role authorization/authentication
   * Flyway for data source control
   * Mockito and JUnit for tests

Objectives
   * Apply Springboot features with others Spring projects
   * Fill knowledge gaps
   * Learn Thymeleaf
   * Have a great wine collection

Missing
   * Finish login / registration flow
   * Check WebSecurity flow
   * Page Styles
   * Main Dashboard page
   
How to Use
   * git clone {git-client/group}/wine-collection.git
   * cd wine-collection
   * mvn spring-boot:run
    
Mappings:
   * GET  - {'/'}              | Get Login Page
   * GET  - {'/login'}         | Get Login Page
   * GET  - {'/registration'}  | Get Registration Page
   * POST - {'/registration'}  | Submit Registration
   * GET  - {'/admin/home'}    | Main Dashboard (Login Success)
